# data_acquisition

## Description

This project handles the us of a Data Acquisition (DAQ) system.
The SCPI protocol is used to communicate instructions to the multimeter by script.
The VISA drivers are used to established the connection with the multimeter.

## Installation

### Windows - C++

- Install Visual Studio (https://visualstudio.microsoft.com/en/)
- Instal IVI Foundation package IviSharedComponent_300.exe (https://www.ivifoundation.org/Shared-Components/default.html)
- Install NI-VISA drivers (https://www.ni.com/en/support/downloads/drivers/download.ni-visa.html)
- Execute the c++ project in visual studio.
- DONE!

### Linux - Python
- (Install NI-VISA drivers (https://www.ni.com/en/support/downloads/drivers/download.ni-visa.html)) -> Not sure necessary.
- In a terminal: 
```bash
echo SUBSYSTEM==\"usb\", MODE=\"0666\", GROUP=\"usbusers\" >/etc/udev/rules.d/99-com.rules
```
- Create a venv and install the requirement file in the "python" folder.
- Execute python/measure.py
- DONE!


