#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pyvisa
import time
import sys
import os
import argparse
import numpy as np
import time

def init_multimeter():
    multimeter_id='USB0::0x05e6::0x6500::04621905::INSTR'
    resource_manager = pyvisa.ResourceManager()
    multimeter = resource_manager.open_resource(multimeter_id)
    print("START")
    
    return multimeter

def measure_current(multimeter, iteration, sampling_time):   
    multimeter.write("*rst; status:preset; *cls")
    multimeter.write("TRACE:POINTS 6000000, \"defbuffer1\"") # Set buffer max size
    multimeter.write(":ROUT:SCAN:BUFF \"defbuffer1\"") # Set buffer1 as main buffer

    multimeter.write("SENS:FUNC 'CURRENT:DC'")
    multimeter.write("SENS:CURR:RANG 0.1")
    
    for i in range(iteration):
        print(multimeter.query("READ?"))
        time.sleep(sampling_tume)



def main(arguments):
    multimeter=init_multimeter()
    measure_current(multimeter, 10, 1)

    
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
